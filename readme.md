Roomba Canvas map for Home Assistant

installation:

1. make directory /homeassistant/www/roomba
2. place index.html and floor.png in /homeassistant/www/roomba

3. make changes to HA configuration.yml:

```
sensor:
  - platform: template
    sensors:
      roomba_status:
              value_template: '{{states.vacuum.roomba.attributes.status}}'

switch:
  - platform: command_line
    switches:
      clear_roomba_log:
        command_on: 'cp /dev/null /config/www/roomba/roomba.log'

notify:
    - name: RoombaFile
      platform: file
      filename: /config/www/roomba/roomba.log
```


4. add automations to HA:

```
- alias: Log Roomba Position
  initial_state: true
  trigger:
      platform: state
      entity_id: "sensor.roomba_location"
  condition: 
    condition: or
    conditions:
      - condition: state
        entity_id: sensor.roomba_status
        state: 'Running'
  action:
    - service: notify.roombafile
      data_template: 
        message: "{{ states.vacuum.roomba.attributes.position }}"
        
- alias: Locate Roomba every 2 seconds
  initial_state: true
  trigger:
    - platform: time_pattern
      seconds: /2
  condition: 
    condition: or
    conditions:
      - condition: state
        entity_id: sensor.roomba_status
        state: 'Running'
  action:
    - service: vacuum.locate
      entity_id: vacuum.roomba

- alias: Write Finished to Log after Cleaning
  initial_state: true
  trigger:
    platform: state
    entity_id: sensor.roomba_status
    to: 'Docked'
  action:
    - service: notify.roombafile
      data_template: 
        message: "Finished"

- alias: Clean Roomba Log
  initial_state: true
  trigger:
    - platform: state
      entity_id: sensor.roomba_status
      to: 'Running'
    - platform: state
      entity_id: sensor.roomba_status
      to: 'New Mission'
  action:
    - service: homeassistant.turn_on
      entity_id: switch.clear_roomba_log
```

5. Add iFrame with url '/local/roomba/index.html' to your Dashboard

